# OpenGL Project
This project's goal was to model an environnement, using OpenGL. Four specific models had to be implemented :
* Dynamic Light : There is a day to night mode as well as a specific light from a lamp.
* Relatistic Camera : It is possible to change from one camera to the other by pressing the space button.
* Random world generation : The world is randomly generated.
* Adding panning move : A car is constantly moving.
## Getting started
### Prerequisites
Before running the application, make sure to have the opengl libraries download with the following command : 
```
sudo apt-get install freeglut3
```

### Compiling the project
The project can be compiled with the following commands :
```
cmake ./
make 
```

### Launching the project
The project can be launched directly
```
./main
```

## Authors
This project was made for an ENSG OpenGL course by F. Geniet and J. Grosmaire.
