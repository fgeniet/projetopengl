# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/formation/projetopengl/ProjetOpenGL/external/soil/src/SOIL.c" "/home/formation/projetopengl/ProjetOpenGL/external/CMakeFiles/SOIL.dir/soil/src/SOIL.c.o"
  "/home/formation/projetopengl/ProjetOpenGL/external/soil/src/image_DXT.c" "/home/formation/projetopengl/ProjetOpenGL/external/CMakeFiles/SOIL.dir/soil/src/image_DXT.c.o"
  "/home/formation/projetopengl/ProjetOpenGL/external/soil/src/image_helper.c" "/home/formation/projetopengl/ProjetOpenGL/external/CMakeFiles/SOIL.dir/soil/src/image_helper.c.o"
  "/home/formation/projetopengl/ProjetOpenGL/external/soil/src/stb_image_aug.c" "/home/formation/projetopengl/ProjetOpenGL/external/CMakeFiles/SOIL.dir/soil/src/stb_image_aug.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "external/glfw-3.3/include/GLFW"
  "external/glew-2.1.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
